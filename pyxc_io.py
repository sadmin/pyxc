#!/usr/bin/env python3
# -*- coding: utf-8 -*-
'''
pyxc - Execute Python code on the command-line shell

@author: sadmin
@license: MIT License
'''


def lazyRead(fileNames):
    ''' Lazy line-by-line file reader '''
    for fileName in fileNames:
        with open(fileName) as f:
            for line in f:
                yield line
