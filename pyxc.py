#!/usr/bin/env python3
# -*- coding: utf-8 -*-
'''
pyxc - Execute Python code on the command-line shell

@author: sadmin
@license: MIT License
'''

# pylint: disable=eval-used
# cmd is considered a trusted source; The user is on the terminal anyway, where he/she can execute arbitrary code

import argparse
from os import linesep  # use from import to avoid slow dot attribute access in loops
import sys

from pyxc_io import lazyRead


def _pyxc(args):
    ''' Apply cmd to stdin '''

    multilineCmd = linesep in args.cmd[0]
    evalFunc = exec if multilineCmd else eval

    # optimize cmd for speed when used in a loop
    cmd = compile(source=args.cmd[0], filename='<string>', mode=evalFunc.__name__)
    lines = sys.stdin if not args.files else lazyRead(args.files)  # Get input
    lines = map(lambda s: s.rstrip(linesep), lines)  # Strip trailing newline characters

    if args.pooled:
        result = evalFunc(cmd, {}, {'lines': list(lines)})  # evaluate cmd

        if not multilineCmd and result is not None:
            print(result)
        
        return

    for line in lines:
        result = evalFunc(cmd, {}, {'line': line, 'lines': lines})  # evaluate cmd

        if not multilineCmd and result is not None:
            print(result)


def main():
    ''' main '''
    parser = argparse.ArgumentParser(
        description='pyxc - Execute Python on the command-line shell', 
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument(
        'cmd', nargs=1,
        help=('Python command for input processing. The variables line and lines are defined magically. All additional '
              'positional arguments are considered to be file names. If cmd is multiline, statements are not printed '
              'to stdout automatically'))
    parser.add_argument('files', nargs='*', help='Files to read, if empty, stdin is used')
    parser.add_argument(
        '-p', '--pooled', action='store_true',
        help=('Consume whole input, convert it to a list and make it available as variable "lines". '
              'Prints to stdout only once, not for each line as in default mode. '
              'Degrades performance, consumes RAM on huge inputs and prevents stream processing'))

    _pyxc(parser.parse_args(sys.argv[1:]))


if __name__ == '__main__':
    main()
