from setuptools import setup

with open("README.md", "r") as f:
    long_description = f.read()

setup(
    name="pyxc",
    version="0.1.1",
    author="sadmin",
    author_email="",
    description="Execute Python code on the command-line shell",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://codeberg.org/sadmin/pyxc",
    classifiers=[
        "Environment :: Console",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
        "Programming Language :: Python :: 3",
        "Topic :: Software Development",
        "Topic :: Utilities",
    ],
    py_modules=["pyxc", "pyxc_io"],
    entry_points={"console_scripts": ["pyxc=pyxc:main"]},
    python_requires=">=3.6",
)
